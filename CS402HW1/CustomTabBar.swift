//
//  CustomTabBar.swift
//  CS402HW1
//
//  Created by Rudy Ruiz on 9/25/18.
//  Copyright © 2018 Rudy Ruiz. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBarController {
    
    var tabBarObject = UITabBarItem();

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.darkGray], for: .normal)
        
        let selectedImage1 = UIImage(named: "vr-glasses")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage1 = UIImage(named: "vr-glasses-black")?.withRenderingMode(.alwaysOriginal)
        tabBarObject = self.tabBar.items![0]
        tabBarObject.image = deSelectedImage1
        tabBarObject.image = selectedImage1
        
        let selectedImage2 = UIImage(named: "chrome-logo")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage2 = UIImage(named: "chrome-logo-black")?.withRenderingMode(.alwaysOriginal)
        tabBarObject = self.tabBar.items![1]
        tabBarObject.image = deSelectedImage2
        tabBarObject.image = selectedImage2
        
        let selectedImage3 = UIImage(named: "mountains")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage3 = UIImage(named: "mountains-black")?.withRenderingMode(.alwaysOriginal)
        tabBarObject = self.tabBar.items![2]
        tabBarObject.image = deSelectedImage3
        tabBarObject.image = selectedImage3
        
        self.selectedIndex = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
